const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const webpack = require("webpack");

module.exports = (env) => {
  console.log("env.file:", env.file);
  return {
    mode: "development",
    entry: `./src/ts/${env.file}`,
    devServer: {
      historyApiFallback: true,
      contentBase: path.resolve(__dirname, "./dist"),
      open: false,
      compress: true,
      hot: true,
      port: 3000,
    },
    resolve: {
      extensions: [".ts", ".js"],
    },
    output: {
      path: path.resolve(__dirname, "./dist"),
      filename: "[name].bundle.js",
    },
    module: {
      rules: [
        {
          test: /\.(scss|css)$/,
          use: [
            "style-loader",
            {
              loader: "css-loader",
              options: {
                importLoaders: 2,
                modules: {
                  localIdentName: "[name]__[local]-[hash:base64:5]",
                },
              },
            },
            "postcss-loader",
            "sass-loader",
          ],
        },
        {
          test: /\.jsx?|.tsx?|.js?|.ts?$/,
          exclude: /(node_modules)/,
          use: {
            loader: "babel-loader",
          },
        },

        {
          test: /\.tsx?|.ts?$/,
          exclude: /(node_modules)/,
          use: {
            loader: "ts-loader",
          },
        },
      ],
    },
    plugins: [
      new CleanWebpackPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new HtmlWebpackPlugin({
        title: "webpack Boilerplate",
        template: path.resolve(
          __dirname,
          `./src/html/${env.html || "index"}.html`
        ), // template file
        filename: "index.html", // output file
      }),
    ],
  };
};
