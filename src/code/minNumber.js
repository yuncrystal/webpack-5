function minNumberOfCoinsForChange(n, denoms) {
  const table = new Array(n + 1).fill(Infinity);
  table[0] = 0;

  for (let num of denoms) {
    for (let i = 0; i <= n; i++) {
      let sum = i + num;
      if (sum <= n) {
        let numOfCoins = table[i] + (sum - i) / num;
        if (numOfCoins < table[sum]) table[sum] = numOfCoins;
      }
    }
  }
  console.log(table);
  return table[n] === Infinity ? -1 : table[n];
}

export { minNumberOfCoinsForChange };
