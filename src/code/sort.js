function quickSort(array) {
  if (array.length < 2) return array;
  quickSortHelper(array, 0, array.length - 1);
  return array;
}

function quickSortHelper(array, left, right) {
  if (left >= right) return array;
  let pivot = array[left];
  let numOfLeft = 0;
  for (let i = left + 1; i <= right; i++) {
    if (pivot > array[i]) {
      numOfLeft++;
      swap(array, i, left + numOfLeft);
    }
  }
  swap(array, left, numOfLeft + left);
  quickSortHelper(array, left, left + numOfLeft - 1);
  quickSortHelper(array, left + numOfLeft + 1, right);
}

function swap(array, i, j) {
  let temp = array[i];
  array[i] = array[j];
  array[j] = temp;
}

const array = quickSort([199, -20, -1, -100, 0, -60, 20]);

console.log("array:", array);
// module.exports =  quickSort // export default



exports.quickSort = quickSort;


