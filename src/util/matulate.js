

function sum(a, b) {
    return a + b
}

function fetchData(callback) {
    return new Promise((resolve, reject) => {
        setTimeout(()=>{
            callback()
            resolve({
                a:'a',
                b:'b',
                c:'c'
            })
        },1000)
    })
}

export {
    sum,
    fetchData
}