import { sum } from '../util/matulate'

test('adds 1+2 to equal 3', () => {
    expect(sum(1, 2)).toBe(3)
})

const can1 = {
    flavor: 'grapefruit',
    ounces: 12,
};
const can2 = {
    flavor: 'grapefruit',
    ounces: 12,
};
//1.  toBe: used to test primitive value or same instance
test('can1 and can2 are not same instance',()=>{
    expect(can1).not.toBe(can2)
})

//2. toEqual: used to compare recursively all properties of object instances
//  deep equality

test('can1 and can2 have all the same properties',()=>{
    expect(can1).toEqual(can2)
})

// 3. Truthiness

// toBeNull 只匹配 null
// toBeUndefined 只匹配 undefined
// toBeDefined 与 toBeUndefined 相反
// toBeTruthy 匹配任何 if 语句为真
// toBeFalsy 匹配任何 if 语句为假


test('null', () => {
    const n = null;
    expect(n).toBeNull();
    expect(n).toBeDefined();
    expect(n).not.toBeUndefined();
    expect(n).not.toBeTruthy();
    expect(n).toBeFalsy();
  });