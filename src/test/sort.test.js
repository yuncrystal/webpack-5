const { quickSort } = require("../code/sort.js");

describe("quicksort", () => {
  test("case #1", () => {
    expect(quickSort([8, 5, 2, 9, 5, 6, 3])).toEqual([2, 3, 5, 5, 6, 8, 9]);
  });
  test("empty input array", () => {
    expect(quickSort([])).toEqual([]);
  });
  test("single input array", () => {
    expect(quickSort([100])).toEqual([100]);
  });
  test("two elements input array", () => {
    expect(quickSort([199, 20])).toEqual([20, 199]);
  });
  test("negative elements in input array", () => {
    expect(quickSort([199, -20, -1, -100, 0, -60, 20])).toEqual(
      [199, -20, -1, -100, 0, -60, 20].sort((a, b) => a - b)
    );
  });
});
