import { getNthFib } from "../ts/algoExpert";

describe('Nth Fibonacci',()=>{
    test('case 0: 0th',()=>{
        expect(getNthFib(0)).toBe(0)
    })
    test('case 1: 1th',()=>{
        expect(getNthFib(1)).toBe(1)
    })
    test('case 2: 2th',()=>{
        expect(getNthFib(2)).toBe(1)
    })
    test('case 3: 3th',()=>{
        expect(getNthFib(3)).toBe(2)
    })
    test('case 4: 4th',()=>{
        expect(getNthFib(4)).toBe(3)
    })
    test('case 99: 99th',()=>{
        expect(getNthFib(11)).toBe(89)
    })
})