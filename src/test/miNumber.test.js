import { minNumberOfCoinsForChange } from "../code/minNumber";

describe("minNumberOfCoinsForChange", () => {
  test("using three denoms [1,5,10] to get 7", () => {
    expect(minNumberOfCoinsForChange(7, [1, 5, 10])).toBe(3);
  });

  test("using three denoms with different order [1,10,5] to get 7", () => {
    expect(minNumberOfCoinsForChange(7, [1, 5, 10])).toBe(3);
  });

  test("using denoms cannot get target n", () => {
    expect(minNumberOfCoinsForChange(1, [2, 5, 10])).toBe(-1);
  });
});
