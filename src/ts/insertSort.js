
function swap(arr, i, j) {
    let temp = arr[i]
    arr[i] = arr[j]
    arr[j] = temp
}

// [0,13,2,13,4]
// 0,13,2
function insertionSort(arr) {
    if (arr.length <= 1) return arr
    for (let i = 1; i < arr.length; i++) {
        for (let j = i; j > 0; j--) {
            if (arr[j] < arr[j - 1]) swap(arr, j, j - 1)
        }
    }
    return arr
}

// console.log('result insertionSort([2,1,9,76,4]):', insertionSort([2, 1, 9, 76, 4]))

