// intersection Types:
// 1.similary to interface inheritance
// 2.for object: basically combine all the properities
// 3.for union types: the types that they have in common

// Object intersection types
type Admin = {
  name: string;
  previlages: string[];
};

type Employee = {
  name: string;
  enrolledDate: Date;
};

const elevatedEmployee: Admin & Employee = {
  name: "Krystal",
  enrolledDate: new Date(),
  previlages: ["admin"],
};

// Type Guards One: check properities in object

type UnknowEmployee = Admin | Employee;

function printEmployee(employee: UnknowEmployee) {
  if ("enrolledDate" in employee) console.log(employee.enrolledDate);
}

// Type Guards Two: use instanceof class

class Car {
  drive() {
    console.log("Driving a car");
  }
}

class Track {
  drive() {
    console.log("Driving a track");
  }

  loadCargo(amount: number) {
    console.log("loadCargo at ", amount);
  }
}

type Vehicle = Track | Car;

function useVehicle(v: Vehicle) {
  v.drive();
  if (v instanceof Track) v.loadCargo(10);
}

const car = new Car()
const track = new Track()

useVehicle(car)
useVehicle(track)

// Union types intersection

type Combinable = string | number;
type Numeric = number | boolean;

type Universal = Combinable & Numeric;

export type { Universal as Universal };
