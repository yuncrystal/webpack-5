import { Node } from "./utils/Node";

class Queue<T> {
  first: Node<T> | null;
  last: Node<T> | null;
  size: number;
  constructor() {
    this.first = null;
    this.last = null;
    this.size = 0;
  }

  enqueue(val: T) {
    const newNode = new Node(val);
    if (!this.last) {
      this.first = newNode;
      this.last = newNode;
    } else {
      this.last.next = newNode;
      this.last = newNode;
    }
    this.size++;
    return this;
  }

  dequeue() {
    if (!this.first) return undefined;
    const temp = this.first;
    if (this.size === 1) this.last = null;
    this.first = temp.next;
    temp.next = null;
    this.size--;
    return temp.value;
  }
}

declare global {
  interface Window {
    queue: Queue<unknown>;
  }
}

const queue = new Queue();
window.queue = queue;
