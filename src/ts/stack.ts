import { Node } from "./utils/Node";

class Stack<T> {
  first: Node<T> | null;
  last: Node<T> | null;
  size: number;
  constructor() {
    this.first = null;
    this.last = null;
    this.size = 0;
  }

  push(val: T) {
    const newNode = new Node(val);
    if (!this.first) {
      this.first = newNode;
      this.last = newNode;
    } else {
      newNode.next = this.first;
      this.first = newNode;
    }
    this.size++;
    return this;
  }

  pop() {
    if (!this.first) return undefined;
    const temp = this.first;
    if (this.size === 1) this.last = null;
    this.first = temp.next;
    temp.next = null;
    this.size--;
    return temp.value;
  }
}

declare global {
  interface Window {
    stack: Stack<unknown>;
  }
}

const stack = new Stack();
window.stack = stack;
