// Generics: Basic theory is, it doesn't care use which exact type

// Built-in generics type: Array and Promise

const names: string[] = ["aaa"];
const fruits: Array<string> = ["apple"];
fruits.forEach((fruit) => fruit.charAt(0));
const nums: Array<number> = [1, 2];

const promise: Promise<number> = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve(10);
  }, 2000);
});

// The below example Generics doesn't care what exact type it is,
// But only care if it has length property
interface Lengthy {
  length: number;
}

function countAndDescribe<T extends Lengthy>(element: T): [T, string] {
  let describe: string = "No value in element";
  if (element.length === 1) describe = "Get 1 element.";
  if (element.length > 1) describe = `Got ${element.length} elements.`;

  return [element, describe];
}

const result = countAndDescribe("sssss");
console.log("result:", result);

// keyof constrain

function extractAndConvert<T extends Object, U extends keyof T>(
  obj: T,
  key: U
): string {
  return "value is " + obj[key];
}

console.log(extractAndConvert({ name: "Krystal", age: 12 }, "name"));

type PrimitiveType = string | number | boolean;

// Generic Class

class DataStorage<T extends PrimitiveType> {
  data: T[] = [];

  addItem(item: T): T[] {
    this.data.push(item);
    return this.data;
  }

  removeItem(item: T): T[] {
    this.data.splice(this.data.indexOf(item), 1);
    return this.data;
  }
}

const data = new DataStorage<number>();
data.addItem(1);
data.addItem(2);
data.addItem(3);
console.log(data.removeItem(2));

// Generic Utility Types

// Readonly Generic

const readOnlyItems: Readonly<string[]> = ["book", "item"];
// readOnlyItems.push('apple')

// Partial Generic

interface Person {
  name: string;
  age: number;
}

function createPerson(name: string, age: number) {
  const person: Partial<Person> = {};
  person.age = age;
  person.name = name;
  const person2: Readonly<Person> = { name, age };
  person2.name = "111";
  return person as Person;
}

const namelist: Readonly<string[]> = ["Max", "Anna"];
namelist.push("sss");

interface Todo {
  title: string;
  description: string;
  completed: boolean;
}

type TodoPreview = Pick<Todo, "title" | "completed">;

const todo: TodoPreview = {
  title: "Clean room",
  completed: false,
};

const todo2: Todo = {
  title: "1",
  description: "2",
  completed: true,
};

type T0 = Extract<"a" | "b" | "c", "a" | "b">;
type T1 = Extract<string | number | (() => void), Function>;
type T2 = Parameters<(a: string, b: number, d: boolean) => void>;
type T3 = Parameters<ErrorConstructor>;
type T4 = ConstructorParameters<ErrorConstructor>;
type T6 = Uppercase<string>;

enum Direction {
  Up = 1,
  Down,
  Left,
  Right,
}

for (let d in Direction) {
  console.log(Direction[d]);
}

let d1: Direction;
d1 = Direction.Up;

type array = string[] | number[];
let a: array = [1, 2];
a.push(1);
export {};
