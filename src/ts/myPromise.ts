class MyPromise<T> {
  state: "pending" | "fulfilled" | "rejected";
  value: T | null;
  resolveFnArr: Function[];
  rejectFnArr: Function[];
  constructor(
    executor: (resolve: (val: T) => void, reject: (val: T) => void) => void
  ) {
    this.state = "pending";
    this.value = null;
    this.resolveFnArr = [];
    this.rejectFnArr = [];

    let resolve = (value: T) => {
      if (this.state !== "pending") return;
      this.value = value;
      this.state = "fulfilled";
      return this;
    };
    let reject = (reason: T) => {
      if (this.state !== "pending") return;
      this.value = reason;
      this.state = "rejected";
      return this;
    };

    try {
      executor(resolve, reject);
    } catch (err) {
      reject(err);
    }
  }

  then(resolveFn: Function, rejectFn: Function) {
    if (typeof resolveFn !== "function") {
      resolveFn = (result: T) => result;
    }
    if (typeof rejectFn !== "function") {
      resolveFn = (result: T) => result;
    }

    return new MyPromise((resolve, reject) => {
      this.resolveFnArr.push();
    });
  }
}

export {};

const promise = new MyPromise((resolve, reject) => {
  resolve(1);
});

declare global {
  interface Window {
    promise: MyPromise<any>;
  }
}

window.promise = promise;

const promise1 = new Promise((resolve, reject) => {
  setTimeout(reject, 2000, "some bad happen");
});
const promise2 = new Promise((resolve) => {
  resolve(promise1);
});

const result = promise2
  .catch((error) => {
    console.log("error:", error);
  });

console.log('promise2:',promise2)
console.log('result:',result)
