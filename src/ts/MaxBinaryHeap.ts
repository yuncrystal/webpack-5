
/**
 * Max Binary Heap, similar to binary search tree
 * The parent always lager then the children
 * Always fill the left side child
 * Use array to implement
 * find child by parent index: 2n+1, 2n+2
 * find parent by child:(n-1)/2 floor
 */

class MaxBinaryHeap<T> {
  values: T[];
  constructor() {
    this.values = [];
  }

  insert(val: T) {
    this.values.push(val);
    this.bubbleUp();
    return this;
  }

  extractMax() {
    if (this.values.length > 1) this.swap(0, this.values.length - 1);
    const removed = this.values.pop();
    this.sinkDown();
    return removed;
  }

  private sinkDown() {
    let currentIndex = 0;
    while (currentIndex < this.values.length - 1) {
      let swapIndex;
      let leftChildIndex = currentIndex * 2 + 1;
      let rightChildIndex = currentIndex * 2 + 2;
      let current = this.values[currentIndex];
      let leftChild;
      if (leftChildIndex < this.values.length) {
        leftChild = this.values[leftChildIndex];
        if (current < leftChild) swapIndex = leftChildIndex;
      }
      if (rightChildIndex < this.values.length && leftChild) {
        let rightChild = this.values[rightChildIndex];
        if (
          (swapIndex === null && current < rightChild) ||
          (swapIndex !== null && leftChild < rightChild)
        ) {
          swapIndex = rightChildIndex;
        }
      }
      if (swapIndex) {
        this.swap(currentIndex, swapIndex);
        currentIndex = swapIndex;
      } else {
        break;
      }
    }
  }

  private bubbleUp() {
    let currentIndex = this.values.length - 1;
    let parentIndex = Math.floor((currentIndex - 1) / 2);
    while (parentIndex >= 0) {
      if (this.values[currentIndex] <= this.values[parentIndex]) break;
      this.swap(currentIndex, parentIndex);
      currentIndex = parentIndex;
      parentIndex = Math.floor((currentIndex - 1) / 2);
    }
  }

  private swap(i: number, j: number) {
    const temp = this.values[i];
    this.values[i] = this.values[j];
    this.values[j] = temp;
  }
}
declare global {
  interface Window {
    maxHeap: MaxBinaryHeap<any>;
  }
}

const maxHeap = new MaxBinaryHeap();
maxHeap.insert(10).insert(2).insert(20).insert(100).insert(9);
window.maxHeap = maxHeap;


export {
    
}