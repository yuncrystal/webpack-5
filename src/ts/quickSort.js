import { swap } from './util'

function pivot(arr, start = 0, end = arr.length) {
    let pivot = arr[start]
    let pivotIndex = start
    for (let i = start + 1; i < end; i++) {
        if (pivot > arr[i]) {
            pivotIndex++
            swap(arr, i, pivotIndex)
        }
    }
    swap(arr, start, pivotIndex)
    return pivotIndex
}

// console.log('result pivot:', pivot([5, 1, 8, 2, 3, 10, 98, 23, 45]))


function quickSort(arr, left = 0, right = arr.length) {
    if (left < right) {
        let pivotIndex = pivot(arr, left, right)
        quickSort(arr, left, pivotIndex - 1)
        quickSort(arr, pivotIndex + 1, right)
    }
    return arr
}

console.log('result quickSort:', quickSort([5, 1, 8, 2, 3, 10, 98, 23, 45]))