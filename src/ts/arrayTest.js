import { swap } from "./util";
/**
 *
 * @param {*} array
 * @param {*} sequence
 */

function isValidSubsequence(array, sequence) {
  let pointerA = 0;
  let pointerS = 0;
  while (pointerS < sequence.length && pointerA < array.length) {
    if (sequence[pointerS] === array[pointerA]) {
      pointerA++;
      pointerS++;
    } else {
      pointerA++;
    }
  }
  if (pointerS === sequence.length) return true;
  else return false;
}

// let array = [5, 1, 22, 25, 6, -1, 8, 10];
// let sequence = [1, -1];
// const isValid = isValidSubsequence(array, sequence);
// console.log("isValid:", isValid);

/**
 *
 * @param {*} array
 * @param {*} targetSum
 */
function twoNumberSum1(array, targetSum) {
  let numbersHash = {};
  let matchedNumPairs = [];
  array.map((num) => {
    numbersHash[num] = num;
  });

  array.map((num) => {
    const matchedNum = targetSum - num;
    if (matchedNum !== num && numbersHash[matchedNum]) {
      matchedNumPairs.push(num);
      matchedNumPairs.push(matchedNum);
      delete numbersHash[num];
      delete numbersHash[matchedNum];
    }
  });
  return matchedNumPairs;
}

/**
 *
 * @param {*} array
 * @param {*} targetSum
 */
function twoNumberSum(array, targetSum) {
  let matchedNumPairs = [];
  if (array.length === 0) return matchedNumPairs;
  array.sort((a, b) => a - b);
  let left = 0,
    right = array.length - 1;
  while (left < right) {
    if (array[left] + array[right] === targetSum) {
      matchedNumPairs.push(array[left], array[right]);
      left++;
      right--;
    } else if (array[left] + array[right] > targetSum) {
      right--;
    } else {
      left++;
    }
  }
  return matchedNumPairs;
}

// let array = [3, 11, -9, 12, 14, 0, 24];
// const pairs = twoNumberSum(array, 3);
// console.log("pairs:", pairs);

/**
 *
 * @param {*} array
 * @param {*} targetSum
 */
function threeNumberSum(array, targetSum) {
  let left, right;
  let pairs = [];
  array.sort((a, b) => a - b);
  for (let i = 0; i < array.length - 2; i++) {
    left = i + 1;
    right = array.length - 1;
    while (left < right) {
      if (array[left] + array[i] + array[right] === targetSum) {
        pairs.push([array[i], array[left], array[right]]);
        left++;
        right--;
      } else if (array[left] + array[i] + array[right] < targetSum) {
        left++;
      } else {
        right--;
      }
    }
  }
  return pairs;
}

// const array = [12, 3, 1, 2, -6, 5, -8, 6];
// const targetSum = 0;
// const pairs = threeNumberSum(array, targetSum);
// console.log(pairs);

function smallestDifference(arrayOne, arrayTwo) {
  arrayOne.sort((a, b) => a - b);
  arrayTwo.sort((a, b) => a - b);
  let i = 0,
    j = 0;
  let pairs = [];
  let smallest = Infinity;
  while (i < arrayOne.length && j < arrayTwo.length) {
    const diff = Math.abs(arrayOne[i] - arrayTwo[j]);
    console.log("i:", i, j);
    if (arrayOne[i] === arrayTwo[j]) {
      pairs[0] = arrayOne[i];
      pairs[1] = arrayTwo[j];
      break;
    }
    if (diff < smallest) {
      smallest = diff;
      pairs[0] = arrayOne[i];
      pairs[1] = arrayTwo[j];
    }
    if (arrayOne[i] > arrayTwo[j]) j++;
    else i++;
  }
  return pairs;
}

// const arrayOne = [12, 3, 1, 2, -6, 5, -8, 6];
// const arrayTwo = [12, 3, 1, 2, -6, 5, -8, 6];
// const difference = smallestDifference(arrayOne, arrayTwo);
// console.log(difference);

function moveElementToEnd(array, toMove) {
  let left = 0,
    right = array.length - 1;

  while (left < right) {
    if (array[right] === toMove) {
      right--;
    } else {
      if (array[left] === toMove) {
        swap(array, left, right);
        right--;
      }
      left++;
    }
  }
  return array;
}
// const array = [5, 1, 2, 5, 5, 3, 4, 6, 7, 5, 8, 9, 10, 11, 5, 5, 12];
// moveElementToEnd(array, 5);
// console.log(array);

function isMonotonic1(array) {
  if (array.length < 2) return true;
  let direction = array[1] - array[0];
  for (let i = 2; i < array.length; i++) {
    if (direction === 0) {
      direction = array[i] - array[i - 1];
    } else {
      if (!notBreakDirection(direction, array[i] - array[i - 1])) {
        return false;
      }
    }
  }
  return true;
}

function notBreakDirection(direction, diff) {
  if (direction > 0) {
    return diff >= 0;
  } else return diff <= 0;
}

function isMonotonic(array) {
  // Declare isNonDecreasing and isNonIncreasing both true
  // iterator if current > previous, isNonIncreasing is false
  //iterator if current < previous, isNonDecreasing is false

  //when iterator over

  let isIncreasing = false,
    isDecreasing = false;
  for (let i = 1; i < array.length; i++) {
    if (array[i] < array[i - 1]) isDecreasing = true;
    if (array[i] > array[i - 1]) isIncreasing = true;
  }
  return !(isIncreasing && isDecreasing);
}
// const array = [];
// const monotonic = isMonotonic(array);
// console.log(monotonic);

function spiralTraverse(array) {
  if (array.length === 0) return [];
  const spiralArray = [];
  let rowStart = 0,
    rowEnd = array.length - 1;
  let columnStart = 0,
    columnEnd = array[0].length - 1;
  while (columnStart <= columnEnd && rowStart <= rowEnd) {
    for (let i = columnStart; i <= columnEnd; i++) {
      spiralArray.push(array[rowStart][i]);
    }
    rowStart++;
    if (rowStart > rowEnd) break;

    for (let j = rowStart; j <= rowEnd; j++) {
      spiralArray.push(array[j][columnEnd]);
    }
    columnEnd--;
    if (columnEnd < columnStart) break;

    for (let i = columnEnd; i >= columnStart; i--) {
      spiralArray.push(array[rowEnd][i]);
    }
    rowEnd--;
    if (rowEnd < rowStart) break;

    for (let j = rowEnd; j >= rowStart; j--) {
      spiralArray.push(array[j][columnStart]);
    }
    columnStart++;
  }
  return spiralArray;
}

// const array = [
//   [1, 2, 3, 4],
//   [10, 11, 12, 5],
//   [9, 8, 7, 6],
// ];
// const spiralArray = spiralTraverse(array);
// console.log(spiralArray);

function longestPeak(array) {
  let a = Infinity;
  if (array.length < 2) return 0;
  let numOfLongest = 0;
  let currentLength = 0;
  let i = 1;
  let isBeforePeak = true;
  while (i < array.length) {
    if (currentLength === 0 && array[i] <= array[i - 1]) {
      i++;
      continue;
    }
    if (isBeforePeak) {
      if (array[i] < array[i - 1]) {
        isBeforePeak = false;
        currentLength = currentLength === 0 ? 2 : currentLength + 1;
      } else if (array[i] > array[i - 1]) {
        currentLength = currentLength === 0 ? 2 : currentLength + 1;
      } else {
        currentLength = 0;
      }
      i++;

      continue;
    }
    if (!isBeforePeak) {
      if (array[i] > array[i - 1]) {
        if (currentLength > numOfLongest) numOfLongest = currentLength;
        currentLength = 2;
        isBeforePeak = true;
      } else if (array[i] < array[i - 1]) {
        currentLength = currentLength === 0 ? 2 : currentLength + 1;
      } else {
        if (currentLength > numOfLongest) numOfLongest = currentLength;
        currentLength = 0;
        isBeforePeak = true;
      }
      i++;
    }
  }
  if (isBeforePeak) {
    currentLength = 0;
  }
  numOfLongest = Math.max(currentLength, numOfLongest);
  return numOfLongest;
}
// const array = [1, 2, 3, 3, 4, 0, 10, 6, 5, -1, -3, 2, 3]
// const longest = longestPeak(array);
// console.log(longest);

function arrayOfProducts(array) {
  const productArray = new Array(array.length);
  productArray.fill(1);
  let productTemp = 1;
  for (let i = 0; i < array.length; i++) {
    productArray[i] *= productTemp;
    productTemp *= array[i];
  }
  productTemp = 1;
  for (let i = array.length - 1; i >= 0; i--) {
    productArray[i] *= productTemp;
    productTemp *= array[i];
  }
  return productArray;
}

// const array = [5, 1, 4, 2]
// const productArray = arrayOfProducts(array);
// console.log(productArray);

function subarraySort(array) {
  let smallestUnsorted = Infinity,
    largestUnsorted = -Infinity;
  let subarrayLeftIndex = 0,
    subarrayRightIndex = array.length - 1;
  for (let i = 0; i < array.length; i++) {
    if (
      (array[i - 1] && array[i] < array[i - 1]) ||
      (array[i + 1] && array[i] > array[i + 1])
    ) {
      if (array[i] < smallestUnsorted) smallestUnsorted = array[i];
      if (array[i] > largestUnsorted) largestUnsorted = array[i];
    }
  }

  if (smallestUnsorted === Infinity) return [-1, -1];

  while (smallestUnsorted >= array[subarrayLeftIndex]) {
    subarrayLeftIndex++;
  }
  while (largestUnsorted <= array[subarrayRightIndex]) {
    subarrayRightIndex--;
  }

  return [subarrayLeftIndex, subarrayRightIndex];
}

// const array = [2, 1];
// const sortArray = subarraySort(array);
// console.log(sortArray);

function largestRange(array) {
  let largestRangeArray = [];
  array.sort((a, b) => a - b);
  let smallestNum = Infinity,
    largestNum = -Infinity;
  for (let i = 0; i < array.length; i++) {
    if (i === 0) {
      smallestNum = array[i];
      largestNum = array[i];
      continue;
    }
    if (array[i] - array[i - 1] === 1) {
      largestNum = array[i];
    } else {
      if (!largestRangeArray.length) {
        largestRangeArray=[smallestNum, largestNum];
      } else {
        if (
          largestNum - smallestNum >
          largestRangeArray[1] - largestRangeArray[0]
        ) {
          largestRangeArray[(smallestNum, largestNum)];
        }
      }
      smallestNum = array[i];
      largestNum = array[i];
    }
  }

  if (
    largestNum - smallestNum >
    largestRangeArray[1] - largestRangeArray[0]
  ) {
    largestRangeArray[(smallestNum, largestNum)];
  }
  return largestRangeArray
}

const array = [1, 11, 3, 0, 15, 5, 2, 4, 10, 7, 12, 6];
const largestR = largestRange(array);
console.log(largestR);
