import classNames from "classnames/bind";
import styles from "../css/iconBar.scss";
const cx = classNames.bind(styles);

const root = document.getElementById("app")!;

type Link = {
  class: string;
  src: string;
};

const links: Link[] = [
  {
    class: "fa fa-home",
    src: "#",
  },
  {
    class: "fa fa-search",
    src: "#",
  },
  {
    class: "fa fa-envelope",
    src: "#",
  },
  {
    class: "fa fa-globe",
    src: "#",
  },
  {
    class: "fa fa-trash",
    src: "#",
  },
];

const createIconBar = (type: string) => {
  const iconbar = document.createElement("div");
  iconbar.className = cx("bar", `${type}-bar`);
  return iconbar;
};

const createLinkElement = (config: Link) => {
  const link = document.createElement("a")!;
  link.href = config.src;
  link.addEventListener("click", () => {
    let siblings;
    if (link && link.parentNode) {
      siblings = link.parentNode.children;
      Array.from(siblings).forEach((element) => {
        element.className = element.className.replace("active", "");
      });
      link.className += cx("active");
    }
  });
  const icon = document.createElement("i");
  icon.className = config.class;
  link.appendChild(icon);
  return link;
};

const iconBarVertical = createIconBar("vertical");
const iconBarHorizontal = createIconBar("horizontal");

const linkList = links.map((link) => createLinkElement(link));
const linkListTwo = links.map((link) => createLinkElement(link));

iconBarVertical.append(...linkList);
iconBarHorizontal.append(...linkListTwo);

root.append(iconBarVertical, iconBarHorizontal);
