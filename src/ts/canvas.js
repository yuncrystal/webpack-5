console.log("r/place");

const canvas = document.querySelector("canvas");
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
const c = canvas.getContext("2d");

// Rect
// c.fillStyle = "rgba(255,0,0,0.1)"; // change the fill
// c.fillRect(0, 0, 50, 50);
// c.fillStyle = "rgba(0,255,0,0.1)"; // change the fill
// c.fillRect(100, 100, 50, 50);
// c.fillStyle = "rgba(0,0,255,0.1)"; // change the fill
// c.fillRect(200, 200, 50, 50);

// //Draw Line

// c.beginPath();
// c.moveTo(50, 200); //start point
// c.lineTo(300, 200); //end point
// c.lineTo(400, 100); //end point
// c.lineTo(400, 400); //end point
// c.strokeStyle = "blue"; //'rgba(22,22,22,1)', '#ffff'
// c.stroke(); // draw line

// //Arc/Circle

// for (let i = 0; i < 10; i++) {
//   let x = Math.random() * window.innerWidth;
//   let y = Math.random() * window.innerHeight;
//   c.beginPath();
//   c.strokeStyle = "green";
//   c.arc(x, y, 30, 0, Math.PI/2, true);
//   c.stroke();
// }

const DEFAULT_RADIUS = 2;
const MAX_RADIUS = 50;
const DISTANCE = 100;

const mouse = {
  x: undefined,
  y: undefined,
};

window.addEventListener("mousemove", (event) => {
  mouse.x = event.x;
  mouse.y = event.y;
});

function Circle(x, y, dx, dy, radius, color) {
  this.x = x;
  this.y = y;
  this.dx = dx;
  this.dy = dy;
  this.radius = radius;

  this.draw = function () {
    c.beginPath();
    c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    c.strokeStyle = color;
    c.fillStyle = color;
    c.stroke();
    c.fill();
  };

  this.update = function () {
    if (this.x + this.radius > innerWidth || this.x - this.radius < 0) {
      this.dx = -this.dx;
    }
    if (this.y + this.radius > innerHeight || this.y - this.radius < 0) {
      this.dy = -this.dy;
    }
    this.y += this.dy;
    this.x += this.dx;
    this.draw();

    if (
      mouse.x - this.x < DISTANCE &&
      mouse.x - this.x > -DISTANCE &&
      mouse.y - this.y < DISTANCE &&
      mouse.y - this.y > -DISTANCE
    ) {
      if (this.radius < MAX_RADIUS) this.radius += 1;
    } else {
      if (this.radius > DEFAULT_RADIUS) this.radius -= 1;
    }
  };
}

const circleArray = [];
for (let i = 0; i < 2; i++) {
  let x = Math.random() * (innerWidth - DEFAULT_RADIUS * 2) + DEFAULT_RADIUS;
  let y = Math.random() * (innerHeight - DEFAULT_RADIUS * 2) + DEFAULT_RADIUS;
  let dx = (Math.random() - 0.5) * 5;
  let dy = (Math.random() - 0.5) * 5;
  var randomColor = Math.floor(Math.random() * 16777215).toString(16);
  circleArray.push(new Circle(x, y, dx, dy, DEFAULT_RADIUS, "#" + randomColor));
}

function animate() {
  requestAnimationFrame(animate);
  c.clearRect(0, 0, innerWidth, innerHeight);
  for (let i = 0; i < circleArray.length; i++) {
    circleArray[i].update();
  }
}

animate();
