// Casting Type One: But this one cannot use in react project
const paragraph = <HTMLParagraphElement>document.getElementById("p1")!;

// Casting Type Two
const input = document.getElementById("input1")! as HTMLInputElement;

// Casting Type Three:use if else, but cannot use casting when declear
// Because if using casting in declear, means the button is not null
const button = document.getElementById("button1");

paragraph.innerText = "I am a paragraph";
input.value = "Input here";

if (button) {
  (button as HTMLButtonElement).innerText = "Click";
}
// ===================
// Index Properties
interface ErrorMessageType {
  id: string; // 因为props id 不是number，是string,所以没有问题
  [props: number]: string; // 所有number的属性都必须是string类型的 1:‘hello’
}

const errorMessages: ErrorMessageType = {
  id: "1",
  1: "ssss",
};
console.log("errorMessages:", errorMessages);

// Index Properties End
// ===================

// ===================
// Function ovaerloads

type Combinable = string | number;

function add(a: number, b: number): number;
function add(a: string, b: string): string;
function add(a: number, b: string): string;
function add(a: string, b: number): string;

// If only write add like this, the return type is also Combinable
function add(a: Combinable, b: Combinable) {
  if (typeof a === "number" && typeof b === "number") {
    return a + b;
  }
  return a.toString() + b.toString();
}

const result = add(1, 4);
const result2 = add("1", "2");
const result3 = add(1, "2");
const result4 = add("1", 2);

// Function ovaerloads ENDS
// ===================

// ===================
// Optional Chaining

const fetchedUserData = {
    id:'u1',
    name:'Krystal',
    job:{title:'ceo',description:'my company'}
}

console.log('fetchedUserData:',fetchedUserData?.job?.title)



// Optional Chaining ENDS
// ===================


// ===================
// Nullish Coalescing : ?? not null or not undefined

const userInput = '' 

const storedData = userInput ?? 'DEFAULT'

console.log('storedData:',storedData)



// Nullish Coalescing ENDS
// ===================

export {};
