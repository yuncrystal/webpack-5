import styles from "../css/accordion.scss";
import classNames from "classnames/bind";
const cx = classNames.bind(styles);

type accordion = {
  title: string;
  content: string;
};

const root = document.getElementById("app")!;

const singleItem = ({ title, content }: accordion) => {
  const item = document.createElement("div");
  const button = document.createElement("button");

  item.appendChild(button);
  button.innerHTML = title;
  button.className = styles.accordionBtn;
  var para = document.createElement("p");
  para.innerHTML = content;
  const panel = document.createElement("div");

  panel.className = cx("panel");
  panel.appendChild(para);
  item.appendChild(panel);

  button.addEventListener("click", () => {
    button.classList.toggle(cx("active"));
    if (panel.style.height) panel.style.height = '0';
    else panel.style.height = panel.scrollHeight + "px";
  });
  return item;
};

const accordionData: accordion[] = [
  {
    title: "Section 1",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
  },
  {
    title: "Section 2",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
  },
  {
    title: "Section 3",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
  },
];
const accordion = accordionData.map((item) => {
  return singleItem(item);
});

accordion.forEach((x) => root.append(x));
