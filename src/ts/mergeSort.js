/**
 * Merge two sorted array to one sorted array
 * @param {*} arr1 sorted array
 * @param {*} arr2 sorted array
 */
function merge(arr1, arr2) {
    let result = [];
    let i = 0, j = 0;
    while (i < arr1.length && j < arr2.length) {
        if (arr1[i] < arr2[j]) {
            result.push(arr1[i]);
            i++
        } else {
            result.push(arr2[j])
            j++
        }
    }
    while (i < arr1.length) {
        result.push(arr1[i]);
        i++
    }
    while (j < arr2.length) {
        result.push(arr2[j])
        j++
    }
    return result
}

function mergeSort(arr) {
    if (arr.length <= 1) return arr;
    let middle = Math.floor(arr.length / 2)
    let left = mergeSort(arr.slice(0, middle))
    let right = mergeSort(arr.slice(middle))
    return merge(left, right)
}

console.log('result mergeSort([2,1,9,76,4]):', mergeSort([2, 1, 9, 76, 4]))
console.log('result mergeSort([10,24,76,73]):', mergeSort([10,24,76,73]))

