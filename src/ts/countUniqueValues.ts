/**
 * Implement a function called countUniqueValues, which accepts a sorted array,
 * and counts the unique values in the array. There can be
 * negative numbers in the array, but it will always be sorted.
 * Time complexity: O(n)
 * Space complexity: O(n)
 */


function countUniqueValues(array: number[]): number {
  if (!array.length) return 0;
  // declare two pointers p1, p2
  let p1: number = 0;
  let p2: number = 1;
  // loop over the array and p2 + 1
  for (p2; p2 < array.length; p2++) {
    // if p1 === p2, p1 move 1 right and set number to the current value
    if (array[p1] !== array[p2]) {
      p1++;
      array[p1] = array[p2];
    }
  }

  return p1+1;
}

const count = countUniqueValues([1,2,3]);
console.log('count:',count)
export {};
