function bestSum(targetSum, numbers, memo = {}) {
  if (targetSum in memo) return memo[targetSum];
  if (targetSum === 0) return [];
  if (targetSum < 0) return null;
  let shortest = null;
  for (let num of numbers) {
    let reminders = targetSum - num;
    let combine = bestSum(
      reminders,
      numbers.filter((x) => x !== num),
      memo
    );

    if (combine) {
      combine = [...combine, num];
      if (shortest === null || combine.length < shortest.length) {
        shortest = combine;
      }
      console.log("combine:", combine);
    }
  }
  memo[targetSum] = shortest;
  return shortest;
}

// const numbers = [1, 2,3,4,5];
// const result = bestSum(10, numbers);
// console.log("result:", result);

function globMatching(fileName, pattern, p1 = 0) {
  if (fileName === "" && p1 === pattern.length) return true;
  if (fileName === "" && p1 < pattern.length) return false;
  if (pattern[p1] === "*") {
    if (fileName.startsWith(pattern[p1 + 1])) {
      p1 = p1 + 2;
      return globMatching(fileName.slice(1), pattern, p1);
    } else {
      return globMatching(fileName.slice(1), pattern, p1);
    }
  } else {
    if (pattern[p1] === "?") {
      return globMatching(fileName.slice(1), pattern, ++p1);
    } else {
      if (fileName.startsWith(pattern[p1])) {
        p1++;
        return globMatching(fileName.slice(1), pattern, p1);
      } else return false;
    }
  }
}

// const pattern = "a*e?g";
// const fileName = "abcdefg";
// const result = globMatching(fileName, pattern);
// console.log("result:", result);

function maxSubsetSumNoAdjacent(array) {
  if (array.length === 0) return 0;
  const maxSumArray = [];
  for (let i = 0; i < array.length; i++) {
    if (i === 1) array[i] = Math.max(array[0], array[1]);
    else if (i > 1) {
      array[i] = Math.max(array[i - 1], array[i - 2] + array[i]);
    }
  }
  return array.pop();
}

// const array = [30, 25, 50, 55, 100, 120];
// const max = maxSubsetSumNoAdjacent(array);
// console.log("max:", max);

function allConstruct(target, wordBank) {
  if (target === "") return [[]]; // means it is one way to the target

  let result = [];
  for (let word of wordBank) {
    if (target.indexOf(word) === 0) {
      let prefixWays = allConstruct(target.slice(word.length), wordBank);
      prefixWays = prefixWays.map((way) => [word, ...way]);
      result.push(...prefixWays);
    }
  }
  return result;
}

// const ways = allConstruct("abcdef", ["ab", "cdef", "ge"]);
// console.log("ways out:", ways);

function fib(n) {
  const table = new Array(n + 1).fill(0);
  table[1] = 1;

  for (let i = 2; i <= n; i++) {
    table[i] = table[i - 1] + table[i - 2];
  }
  console.log("table:", table);
  return table[n];
}

// console.log(fib(6));

function gridTraveler(n) {
  const table = new Array(n + 1).fill().map(() => new Array(n + 1).fill(0));
  table[1][1] = 1;
  for (let i = 1; i <= n; i++) {
    for (let j = 1; j <= n; j++) {
      if (!(i === 1 && j === 1))
        table[i][j] = table[i - 1][j] + table[i][j - 1];
    }
  }
  return table[n][n]
}

const differentWays = gridTraveler(3);
console.log('differentWays:',differentWays)
