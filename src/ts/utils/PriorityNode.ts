interface NodeType<T> {
  value: T;
  priority: number; // lower number higher priority
}

export class Node<T> implements NodeType<T> {
  value: T;
  priority: number;

  constructor(value: T, priority: number) {
    this.value = value;
    this.priority = priority;
  }
}
