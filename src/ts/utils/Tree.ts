interface NodeType<T> {
  value: T;
  left: NodeType<T> | null;
  right: NodeType<T> | null;
}

export class Node<T> implements NodeType<T> {
  value: T;
  left: Node<T> | null = null;
  right: Node<T> | null = null;

  constructor(value: T) {
    this.value = value;
    this.left = null;
    this.right = null;
  }
}
