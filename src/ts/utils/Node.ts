interface NodeType<T> {
  value: T;
  next: NodeType<T> | null;
}

export class Node<T> implements NodeType<T> {
  value: T;
  next: Node<T> | null = null;

  constructor(value: T) {
    this.value = value;
  }
}
