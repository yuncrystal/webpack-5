fetch("https://reqres.in/api/users?page=2")
  .then((res) => res.json())
  .then((json) => {
    console.log("json:", json);
  });

  async function fetchData2() {
    return await new Promise((resolve) => {
      resolve(2);
        console.log("p2 resolve");
    });
  }
  
  async function fetchData() {
    await new Promise((resolve) => {
     resolve(1);
        console.log("p1 resolve");
        await fetchData2();
    });
  }
  
  await fetchData();
  console.log("finish");