import { Node } from "./utils/Tree";

class BinarySearchTree<T> {
  root: Node<T> | null = null;
  constructor() {
    this.root = null;
  }

  insert(val: T) {
    const newNode = new Node(val);
    let current = this.root;
    if (!current) {
      this.root = newNode;
      return this;
    } else {
      while (true) {
        if (val < current.value) {
          if (current.left) current = current.left;
          else {
            current.left = newNode;
            return this;
          }
        } else {
          if (current.right) current = current.right;
          else {
            current.right = newNode;
            return this;
          }
        }
      }
    }
  }

  find(val: T) {
    let current = this.root;
    while (current) {
      if (val === current.value) {
        return current;
      } else if (val < current.value) {
        current = current.left;
      } else {
        current = current.right;
      }
    }
    return false;
  }

  breathFirstTraversal() {
    const queue: Node<T>[] = [];
    const nodeArray: T[] = [];
    if (this.root) queue.push(this.root);
    while (queue.length) {
      let node = queue.shift();
      nodeArray.push(node!.value);
      if (node!.left) queue.push(node!.left);
      if (node!.right) queue.push(node!.right);
    }
    return nodeArray;
  }

  //      10
  //    5     13
  //  2   6  12  19

  DFSInOrder() {
    const ordered: T[] = [];
    const current = this.root;
    if (current) this.inOrderVisit(current, ordered);
    return ordered;
  }

  DFSPreOrder() {
    const ordered: T[] = [];
    const current = this.root;
    if (current) this.preOrderVisit(current, ordered);
  }
  DFSPostOrder() {
    const ordered: T[] = [];
    const current = this.root;
    if (current) this.postOrderVisit(current, ordered);
  }

  private postOrderVisit(node: Node<T>, ordered: T[]) {
    if (node.left) this.inOrderVisit(node?.left, ordered);
    if (node.right) this.inOrderVisit(node.right, ordered);
    ordered.push(node.value);
  }
  private preOrderVisit(node: Node<T>, ordered: T[]) {
    ordered.push(node.value);
    if (node.left) this.inOrderVisit(node?.left, ordered);
    if (node.right) this.inOrderVisit(node.right, ordered);
  }

  private inOrderVisit(node: Node<T>, ordered: T[]) {
    if (node.left) this.inOrderVisit(node?.left, ordered);
    ordered.push(node?.value);
    if (node.right) this.inOrderVisit(node.right, ordered);
  }
}

declare global {
  interface Window {
    tree: BinarySearchTree<any>;
  }
}

const tree = new BinarySearchTree();
tree.insert(10).insert(6).insert(15).insert(13).insert(3).insert(8).insert(20);
window.tree = tree;
