const unsortedArray = [11, 10, 2, 3, 6, 7, 199];

// ************** 三个低效率的sorting algorithm ******************//
// ************** Bubble, Selection, Insertion Sort ******************//
/**
 * @param array
 * 最好O(n)
 * 最坏和平均O(n*n)
 */
function bubbleSort<T>(array: T[]): T[] {
  if (array.length === 1) return array;
  //   Declare swapped, if no element need to swap anymore, quit the loop
  let swapped = false;
  for (let i = 0; i < array.length - 1; i++) {
    swapped = false;
    for (let j = 0; j < array.length - 1; j++) {
      // swap the largest
      if (array[j] > array[j + 1]) {
        swap(array, j, j + 1);
        swapped = true;
      }
    }
    if (!swapped) return array;
  }
  return array;
}

const bubbleSortedArray = bubbleSort(unsortedArray);
console.log("bubbleSortedArray:", bubbleSortedArray);

/**
 * @param array
 * 最好O(n*n)
 * 最坏和平均O(n*n)
 */
function selectionSort() {}

/**
 * @param array
 * 最好O(n)
 * 最坏和平均O(n*n)
 */
function insertionSort() {}

function swap<T>(array: T[], i: number, j: number) {
  const temp = array[i];
  array[i] = array[j];
  array[j] = temp;
}

// ************** 三个中等效率的sorting algorithm ******************//
// ************** Bubble, Selection, Insertion Sort ******************//

function mergeSort() {}


/**
 *  Merge function: merge 的两个参数必须是排序的array
 * @param arr1 
 * @param arr2 
 */
function merge(arr1: number[], arr2: number[]): number[] {
  const mergedArray = [];
  let i = 0;
  let j = 0;

  while (i < arr1.length && j < arr2.length) {
    if (arr1[i] < arr2[j]) {
      mergedArray.push(arr1[i]);
      i++;
    } else {
      mergedArray.push(arr2[j]);
      j++;
    }
  }

  //   one of array is no longer has element, but another one still have element

  while (i < arr1.length) {
    mergedArray.push(arr1[i]);
    i++;
  }
  while (j < arr2.length) {
    mergedArray.push(arr2[j]);
    j++;
  }

  return mergedArray;
}

const subArray = [1,2,10,4]
const subArray2 = [110,12,1,9,11]

const mergedArray = merge(subArray,subArray2)
console.log('mergedArray:',mergedArray)


export { bubbleSort, selectionSort, insertionSort,mergeSort };
