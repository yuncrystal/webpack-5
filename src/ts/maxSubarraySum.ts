function maxSubarraySum(arr: number[], num: number):number {
  // declare maxSum, tempSum
  if (arr.length === 0) return 0;
  let maxSum = 0;
  let tempSum = 0;
  let p1 = 0;
  for (let i = 0; i < arr.length; i++) {
    // if window diff is equal to num, i and p1 both move right 1,calculate the sum
    if (i - p1  === num) {
        tempSum = tempSum + arr[i] - arr[p1]
        if(tempSum > maxSum) maxSum  = tempSum
        i++
      // if window diff is smaller then num, plus the new element to maxSum and tempSum
    } else {
      tempSum += arr[i];
    }
  }
  return maxSum
}
const maxSum = maxSubarraySum([1, 3, 5, 3, 2, 10, 5, 2, 3], 4);
console.log('maxSum:',maxSum)





export {};
