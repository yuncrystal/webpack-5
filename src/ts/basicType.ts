// 1. Core Types: primitive type are all lowercase
// number, string, boolean, object, Array, Tuple, Enum

function add(num1: number, num2: number, showResult: boolean, phrase: string) {
  const result = num1 + num2;
  if (showResult) {
    console.log(phrase + result);
  } else {
    return result;
  }
}

const num1 = 1;
const num2 = 2;
const showResult = false;
const phrase = "The result is ";
const num3 = add(num1, num2, showResult, phrase);

// Type enum
enum Role {
  ADMIN = 10,
  AUTHOR,
  READ_ONLY,
}

const person: {
  name: string;
  age: number;
  role: [number, string];
  hobbies: string[];
} = {
  name: "Krystal",
  age: 20,
  role: [Role.ADMIN, "admin"],
  hobbies: ["swimming", "cooking"],
};

console.log("person:", person);

// Example of Union Types and Literal Types and Type Aliase

type Combinable = number | string;
type ConversionDescriptor = "as-text" | "as-number"; //Literal Types

function combined(
  input1: string | number, //Union Types
  input2: Combinable, // Type Aliase
  resultConvertion: ConversionDescriptor
) {
  if (
    (typeof input1 === "number" && typeof input2 === "number") ||
    resultConvertion === "as-number"
  )
    return +input1 + +input2;
  else return input1.toString() + input2.toString();
}

const combinedString = combined("String", "is", "as-text");
const combinedNum = combined(1, 3, "as-number");

// Function return type

function printResult(num: number): void {
  console.log("result:", num);
}

// Functions as Types

function minus(num1: number, num2: number) {
  return num1 - num2;
}

// The variable must be a function
let combineValues: Function;

// The variable take 2 number params and return a number
let combineValues2: (a: number, b: number) => number;

combineValues2 = minus; //works

// combineValues2 = printResult; //not works: because of wrong param type

// Callback function

function addAndHandle(num1: number, num2: number, cb: (num: number) => void) {
  const result = num1 + num2;
  cb(result);
}

addAndHandle(1, 10, (result) => {
  console.log("result:", result);
});

// Type Unknow: when assign unkonwn type to other variable,
// It will check if these current unkonwn type is correct

let userInput: unknown;
let userName: string;

userName = "ssss";
if (typeof userInput === "string") {
  userName = userInput;
}

// The never type: when console log the result, it will not show undefined

function generateError(message: string, errorCode: number): never {
  throw {
    message,
    errorCode,
  };
}

// const error = generateError("server error", 500);

export { add, minus, combined };

const renderRadio = <T extends string, V extends string>(
  enumVariable: { [key in T]: V },
  status: V,
  name: string
) => {
  return Object.keys(enumVariable).map((option, index) => console.log("a"));
};

function merge<T extends object, U extends object>(a: T, b: U) {
  return Object.assign(a, b);
}

const mergedObj = merge({ name: "name", age: "age" }, { sex: "female" });

function getValue<T extends object, U extends keyof T>(obj: T, key: U) {
  return obj[key];
}

getValue({ name: "yun" }, "name");
