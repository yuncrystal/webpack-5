function maxSubArray(array) {
  if (array.length === 0) return null;
  let maxSum = array[0];
  for (let i = 1; i < array.length; i++) {
    array[i] = Math.max(array[i], array[i] + array[i - 1]);
    if (maxSum < array[i]) maxSum = array[i];
  }
  return maxSum;
}

// maxSubArray([-2, 1, -3, 4, -1, 2, 1, -5, 4]);

function merge(arr1, arr2) {
  let i = 0,
    j = 0;
  let merged = [];
  while (i < arr1.length && j < arr2.length) {
    if (arr1[i] < arr2[j]) {
      merged.push(arr1[i]);
      i++;
    } else {
      merged.push(arr2[j]);
      j++;
    }
  }

  while (i < arr1.length) {
    merged.push(arr1[i]);
    i++;
  }
  while (j < arr2.length) {
    merged.push(arr2[j]);
    j++;
  }
  return merged;
}

function mergeSort(array) {
  if (array.length <= 1) return array;
  const mid = Math.floor(array.length / 2);
  const left = mergeSort(array.slice(0, mid));
  const right = mergeSort(array.slice(mid));
  return merge(left, right);
}

function breadthFirstSearch(array) {
  let queue = [...array];
  let breadthVisitedArray = [];
  while (queue.length) {
    const node = queue.shift();
    breadthVisitedArray.push(node.name);
    queue.push(...node.children);
  }
  return breadthVisitedArray
}

const merged = mergeSort([8, 5, 2, 9, 5, 6, 3]);
