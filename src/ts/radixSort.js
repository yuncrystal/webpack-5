
/**
 * 返回num的第place位数字，从右向左 0-10....
 * @param {*} num 
 * @param {*} place 
 */
// function getDigit(num, place) {
//     let str = num + ''
//     let index = str.length - 1 - place
//     if (index < 0) return 0
//     else return str[index]
// }

function getDigit(num, place) {
    return Math.floor(Math.abs(num) / Math.pow(10, place)) % 10
}

function digitCount(num) {
    if (num === 0) return 0;
    return Math.floor(Math.log10(Math.abs(num))) + 1
}

function mostDigit(arr) {
    let maxDigit = 0;
    for (let i = 0; i < arr.length; i++) {
        maxDigit = Math.max(maxDigit, digitCount(arr[i]))
    }
    return maxDigit
}

function radixSort(arr){
    const maxDigitCount = mostDigit(arr)
}

// console.log('result getDigit(7323,0):', getDigit(7323, 0))
// console.log('result getDigit(7323,1):', getDigit(7323, 1))
// console.log('result getDigit(7323,2):', getDigit(7323, 2))
// console.log('result getDigit(7323,3):', getDigit(7323, 3))
// console.log('result getDigit(7323,4):', getDigit(7323, 4))
// console.log('result mostDigit([10,231,342234,33]):', mostDigit([10, 231, 342234, 33]))
console.log('result radixSort([10,231,342234,33]):', radixSort([10, 231, 342234, 33]))