import styles from "../css/slider.scss";
import classNames from "classnames/bind";
const cx = classNames.bind(styles);

type Slider = {
  index: number;
  imgUrl: string;
  title: string;
};

const sliderData: Slider[] = [
  {
    index: 1,
    imgUrl: "https://www.w3schools.com/howto/img_nature_wide.jpg",
    title: "Caption One",
  },
  {
    index: 2,
    imgUrl: "https://www.w3schools.com/howto/img_snow_wide.jpg",
    title: "Caption two",
  },
  {
    index: 3,
    imgUrl: "https://www.w3schools.com/howto/img_mountains_wide.jpg",
    title: "Caption Three",
  },
];

const root = document.getElementById("app")!;

const createSingleSlider = ({ index, imgUrl, title }: Slider) => {
  const slider = document.createElement("div");
  slider.className = cx("slide");

  const sliderIndex = document.createElement("span");
  sliderIndex.innerText = index.toString();

  const img = document.createElement("img");
  img.src = imgUrl;

  const h1 = document.createElement("h1");
  h1.innerHTML = title;

  slider.appendChild(sliderIndex);
  slider.appendChild(img);
  slider.appendChild(h1);
  return slider;
};

const createDot = ({ index }: Slider) => {
  const dot = document.createElement("span");
  // dot.key = index
  dot.className = cx("dot");
  dot.addEventListener("click", () => {
    // console.log('show slider:', dot.key)
  });
  return dot;
};

const sliderContainer = document.createElement("div");
const dotContainer = document.createElement("div");
dotContainer.className = cx("dot-wrap");

const sliders = sliderData.map((slider) => createSingleSlider(slider));
const dots = sliderData.map((dot) => createDot(dot));

sliders.forEach((x) => sliderContainer.append(x));
dots.forEach((x) => dotContainer.append(x));

root.append(sliderContainer);
root.append(dotContainer);
