function reverse(str) {
  return str.splice("").reverse().join("");
}

function reverse2(str) {
  let reversed = "";
  for (let i = str.length; i >= 0; i--) {
    reversed += str[i];
  }
  return reversed;
}
function reverseInt(num) {
  return (
    Math.sign(num) *
    parseInt(Math.abs(num).toString().split("").reverse().join(""))
  );
}

function chunk(array, size) {
    const chunkedArray = [];
    for (let i = 0; i < array.length; i=i + 2) {
      chunkedArray.push([array.slice(i,i+size)])
    }
    return chunkedArray
  }
