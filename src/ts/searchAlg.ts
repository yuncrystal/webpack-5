/**
 * 最普通的search，使用便利
 * Big O: O(n)
 * @param array
 * @param value
 */
function linearSearch(array: any[], value: any): number {
  // add whatever parameters you deem necessary - good luck!
  for (let i = 0; i < array.length; i++) {
    if (array[i] === value) return i;
  }
  return -1;
}

/**
 * Binary search: 对半搜索，效率高，只用于已经排序的数组
 * Big O: O(log n)
 * @param array: must be sorted array
 * @param value
 * [1,2,3,4,5,6],4
 */
function binarySearch(array: any[], value: any): number {
  // declare two pointers
  if (array.length === 0 || !value) return -1;
  let left = 0;
  let right = array.length - 1;

  while (left <= right) {
    let pointer = Math.floor((right + left) / 2);
    if (array[pointer] === value) return pointer;
    else if (value > array[pointer]) left = pointer + 1;
    else right = pointer - 1;
  }

  return -1;
}





export {};
