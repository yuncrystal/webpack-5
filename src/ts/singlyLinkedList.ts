import { Node } from "./utils/Node";

class SinglyLinkedList<T> {
  head: Node<T> | null = null;
  tail: Node<T> | null = null;
  length: number;
  constructor() {
    this.length = 0;
  }

  traverse() {
    let current = this.head;
    while (current) {
      console.log(current.value);
      current = current.next;
    }
  }

  /**
   * Push: add node at tail
   * O(1)
   * @param val
   */
  push(val: T): this {
    if (!this.tail) {
      this.insertFirst(val);
    } else {
      var node = new Node(val);
      this.tail.next = node;
      this.tail = node;
    }
    this.length++;
    return this;
  }

  pop(): Node<T> | undefined {
    if (!this.head) return undefined;
    let current = this.head;
    let newTail = current;
    while (current.next) {
      newTail = current;
      current = current.next;
    }
    newTail.next = null;
    this.tail = newTail;
    this.length--;
    if (this.length === 0) {
      this.tail = null;
      this.head = null;
    }
    return current;
  }

  insertFirst(val: T) {
    var node = new Node(val);
    this.head = node;
    this.tail = node;
    return true;
  }

  /**
   * Remove from head
   */
  shift(): Node<T> | undefined {
    if (!this.head) return undefined;
    const currentHead = this.head;
    this.head = currentHead.next;
    currentHead.next = null;
    this.length--;
    if (this.length === 0) {
      this.head = null;
      this.tail = null;
    }
    return currentHead;
  }

  unshift(val: T): this {
    if (!this.head) {
      this.insertFirst(val);
    } else {
      const node = new Node(val);
      node.next = this.head;
      this.head = node;
    }
    this.length++;
    return this;
  }

  get(num: number): Node<T> | undefined {
    if (this.head && num < this.length) {
      let current: Node<T> = this.head;
      let currentIndex = 0;
      while (current.next && currentIndex < num) {
        current = current.next;
        currentIndex++;
      }
      return current;
    } else return undefined;
  }

  set(index: number, val: T): boolean {
    const foundNode = this.get(index);
    if (foundNode) {
      foundNode.value = val;
      return true;
    } else return false;
  }

  insert(index: number, val: T): boolean {
    if (index < 0 || index > this.length) return false;
    if (index === this.length) return !!this.push(val);
    if (index === 0) return !!this.unshift(val);
    const prev = this.get(index - 1);
    const newNode = new Node(val);
    if (prev) {
      newNode.next = prev.next;
      prev.next = newNode;
      this.length++;
      return true;
    } else {
      return false;
    }
  }

  remove(index: number) {
    if (index < 0 || index >= this.length) return undefined;
    if (index === 0) return !!this.shift();
    if (index === this.length - 1) return !!this.pop();
    const foundPrev = this.get(index - 1);
    const removedNode = foundPrev?.next;
    if (foundPrev && removedNode) {
      foundPrev.next = removedNode.next;
      return true;
    } else return false;
  }

  reverse() {
    let current = this.head;
    this.head = this.tail;
    this.tail = current;
    let prev: Node<T> | null = null;
    let next: Node<T> | null;

    for (let i = 0; i < this.length; i++) {
      if (current) {
        next = current.next;
        current.next = prev;
        prev = current;
        current = next;
      }
    }
    return this;
  }
  // 1 -> 2 ->3 ->4
  // <-1 -> 2 ->3 ->4
  //     p    c   n
}

const list1 = new SinglyLinkedList();
list1.push("1").push("2").push("3").push("4").push("5");
// list1.traverse();
// list1.shift();
// console.log("list1.pop():", list1.pop());

// console.log("The ind ex of 2 is: ", list1.get(2));
list1.reverse();
console.log("list1:", list1.traverse());
