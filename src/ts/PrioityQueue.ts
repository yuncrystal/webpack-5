import { Node } from "./utils/PriorityNode";
/**
 * Priority Queue, use Max Binary Heap to implement
 * Max Binary Heap, similar to binary search tree
 * The parent always smaller then the children
 * Always fill the left side child
 * Use array to implement
 * find child by parent index: 2n+1, 2n+2
 * find parent by child:(n-1)/2 floor
 */

class PriorityQueue<T> {
  values: Node<T>[];
  constructor() {
    this.values = [];
  }

  enqueue(val: T, priority: number) {
    const node = new Node<T>(val, priority);
    this.values.push(node);
    this.bubbleUp();
    return this;
  }

  dequeue() {
    if (this.values.length > 1) this.swap(0, this.values.length - 1);
    const removed = this.values.pop();
    this.sinkDown();
    return removed;
  }

  private sinkDown() {
    let currentIndex = 0;
    while (currentIndex < this.values.length - 1) {
      let swapIndex;
      let leftChildIndex = currentIndex * 2 + 1;
      let rightChildIndex = currentIndex * 2 + 2;
      let current = this.values[currentIndex].priority;
      let leftChild;
      if (leftChildIndex < this.values.length) {
        leftChild = this.values[leftChildIndex].priority;
        if (current < leftChild) swapIndex = leftChildIndex;
      }
      if (rightChildIndex < this.values.length && leftChild) {
        let rightChild = this.values[rightChildIndex].priority;
        if (
          (swapIndex === null && current < rightChild) ||
          (swapIndex !== null && leftChild < rightChild)
        ) {
          swapIndex = rightChildIndex;
        }
      }
      if (swapIndex) {
        this.swap(currentIndex, swapIndex);
        currentIndex = swapIndex;
      } else {
        break;
      }
    }
  }

  private bubbleUp() {
    let currentIndex = this.values.length - 1;
    let parentIndex = Math.floor((currentIndex - 1) / 2);
    while (parentIndex >= 0) {
      if (
        this.values[currentIndex].priority <= this.values[parentIndex].priority
      )
        break;
      this.swap(currentIndex, parentIndex);
      currentIndex = parentIndex;
      parentIndex = Math.floor((currentIndex - 1) / 2);
    }
  }

  private swap(i: number, j: number) {
    const temp = this.values[i];
    this.values[i] = this.values[j];
    this.values[j] = temp;
  }
}
declare global {
  interface Window {
    priorityQueue: PriorityQueue<any>;
  }
}

const priorityQueue = new PriorityQueue();
priorityQueue
  .enqueue("优先级最高", 20)
  .enqueue("优先级高", 19)
  .enqueue("优先级中等", 5)
  .enqueue("优先级最低", 2)
  .enqueue("优先级低", 1);
window.priorityQueue = priorityQueue;
