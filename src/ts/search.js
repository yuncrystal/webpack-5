function binarySearch(arr, search) {
    if (arr.length === 0) return -1
    let left = 0, right = arr.length - 1;
    while (left < right) {
        let middle = Math.floor((left + right) / 2)
        if (arr[middle] === search) return middle
        if (search < arr[middle]) right = middle - 1
        if (search > arr[middle]) left = middle + 1
    }
    return -1
}