interface Vertex {
  node: string;
  weight: number;
}

interface PreviousNodeType {
  [props: string]: Vertex;
}
interface AdjacencyList {
  [props: string]: Vertex[];
}

class Graph {
  adjacencyList: AdjacencyList;
  constructor() {
    this.adjacencyList = {};
  }

  addVertex(vertex: string) {
    if (!this.adjacencyList.hasOwnProperty(vertex)) {
      this.adjacencyList[vertex] = [];
    }
    return this;
  }

  addEdge(vertex1: string, vertex2: string, weight: number) {
    if (this.adjacencyList[vertex1] && this.adjacencyList[vertex2]) {
      this.adjacencyList[vertex1].push({ node: vertex2, weight });
      this.adjacencyList[vertex2].push({ node: vertex1, weight });
    }
    return this;
  }

  /**
   *
   * @param vertex1
   * @param vertex2
   *
   */
  Dijkstra(vertex1: string, vertex2: string) {
    // Declare 3 variables
    // const visited: string = [];
    // const previousNode: PreviousNodeType = {};
    // const distances = {}

  }
}

declare global {
  interface Window {
    graph: any;
  }
}

const graph = new Graph();
graph
  .addVertex("A")
  .addVertex("B")
  .addVertex("C")
  .addVertex("D")
  .addVertex("E")
  .addVertex("F");
graph
  .addEdge("A", "B", 4)
  .addEdge("A", "C", 2)
  .addEdge("B", "E", 3)
  .addEdge("C", "D", 2)
  .addEdge("C", "F", 4)
  .addEdge("D", "E", 3)
  .addEdge("D", "F", 1)
  .addEdge("E", "F", 1);

graph.Dijkstra("A", "E");
window.graph = graph;
export {};
