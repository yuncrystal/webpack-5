function getNthFib(n, memo = {}) {
  if (n in memo) return memo[n];
  if (n === 0) return 0;
  if (n === 1) return 1;
  memo[n] = getNthFib(n - 1, memo) + getNthFib(n - 2, memo);
  return memo[n];
}

console.log("getNthFib:", getNthFib(6));

export { getNthFib };
class Node {
  constructor(name) {
    this.name = name;
    this.children = [];
  }

  addChild(name) {
    this.children.push(new Node(name));
    return this;
  }

  depthFirstSearch() {
    const depthFirstElements = [];
    depthFirstSearchHelper(this, depthFirstElements);
    return depthFirstElements;
  }

  depthFirstSearchHelper(node, depthFirstElements) {
    depthFirstElements.push(node.name);
    for (child of node.children) {
      depthFirstSearchHelper(child, depthFirstElements);
    }
  }
}
