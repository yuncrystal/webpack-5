//Interface can extends several interface
interface Named {
  name: string;
}

interface CanFly extends Named {
  flySpeed: number;
  fly(): void;
}

interface CanSwim extends Named {
  swimSpeed: number;
  swim(): void;
}

interface CanDrive extends Named {
  driveSpeed: number;
  drive(): void;
}

interface CanWalk extends Named {
  walkSpeed: number;
  walk(): void;
}

interface Human extends CanWalk, CanDrive, CanSwim {
  age: number;
  sayHi(): void;
}

// when class A extends a abstract class,
// class A must implement the abstract method
abstract class People implements Named {
  name: string;
  constructor(name: string) {
    this.name = name;
  }

  dance(): void {
    console.log(`${this.name} can dance`);
  }

  abstract speach(): void;
}

//class can only extends one class,and need to implement super()
// class can implements several Interfaces
class Employee extends People implements Human {
  age: number;
  id: string;
  salary?: number;
  walkSpeed: number;
  driveSpeed: number;
  swimSpeed: number;

  constructor(
    name: string,
    id: string,
    age: number,
    walkSpeed: number = 10,
    driveSpeed: number = 50,
    swimSpeed: number = 20
  ) {
    super(name);
    this.id = id;
    this.age = age;
    this.walkSpeed = walkSpeed;
    this.driveSpeed = driveSpeed;
    this.swimSpeed = swimSpeed;
  }

  swim(): void {
    console.log(`${this.name} swim at ${this.swimSpeed} !`);
  }

  speach(): void {
    console.log(`${this.name} speach!`);
  }
  sayHi(): void {
    console.log(`${this.name} say hi !`);
  }
  walk(): void {
    console.log(`${this.name} walk at ${this.walkSpeed}!`);
  }
  drive(): void {
    console.log(`${this.name} drive at ${this.driveSpeed} Km/h !`);
  }

  describe() {
    console.log(`${this.name}'s age is ${this.age}`);
  }
}

const p = new Employee("Krystal", "0001", 30, 20);

p.describe();
p.drive();
p.speach();
p.dance();

//Discriminate Union

interface Bird {
  type: "bird";
  flySpeed: number;
}

interface Fish {
  type: "fish";
  swimSpeed: number;
}

type Animal = Bird | Fish;

function moveAnimal(animal: Animal) {
  switch (animal.type) {
    case "bird":
      console.log(`${animal.type} move at:`, animal.flySpeed);
      break;
    case "fish":
      console.log(`${animal.type} move at:`, animal.swimSpeed);
      break;
  }
}

moveAnimal({ type: "bird", flySpeed: 10 });

// interface as function type
// type AddFun = (a: number, b: number)=> number

// same as use type
interface AddFun {
  (a: number, b: number): number;
}

let add: AddFun = (n1: number, n2: number) => {
  return n1 + n2;
};

console.log(add(1, 3));

export { Employee };
