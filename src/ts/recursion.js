
/**
 * 累加
 * @param {*} num 
 */
function sumRange(num) {
    if (num === 1) return 1;
    return num + sumRange(num - 1)
}

// sumRange(5)

/**
 * 累乘
 * @param {*} num 
 */
function factorial(num) {
    if (x < 0) return 0
    if (num <= 1) return 1;
    return num * factorial(num - 1)
}


// console.log('result factorial(0):', factorial(0))
// console.log('result factorial(1):', factorial(1))
// console.log('result factorial(2):', factorial(2))
// console.log('result factorial(4):', factorial(4))
// console.log('result factorial(7):', factorial(7))

// power(2,0) //1
// power(2,2) //4
// power(2,4) //16

function power(base, exponent) {
    if (exponent === 0) return 1
    return base * power(base, exponent - 1)
}

// console.log('result power(2,0):', power(2, 0))
// console.log('result power(2,1):', power(2, 1))
// console.log('result power(2,2):', power(2, 2))
// console.log('result power(2,3):', power(2, 3))
// console.log('result power(2,4):', power(2, 4))

/**
 * 计算array相乘
 * @param {*} arr 
 */
function productOfArray(arr) {
    if (arr.length === 1) return arr[0]
    if (arr[0] === 0) return 0
    return arr[0] * productOfArray(arr.slice(1))
}
// console.log('result productOfArray([1,2,3]):', productOfArray([1,2,3]))
// console.log('result productOfArray([1,2,3,10]):', productOfArray([1,2,3,10]))


// 
// 
/**
 * Return the nth number in Fibonacci sequence 
 * Fibonacci sequence : 1,1,2,3,5,8
 */
function fib(num) {
    if (num <= 2) return 1;
    return fib(num - 1) + fib(num - 2)
}
// console.log('result fib(1):', fib(1))
// console.log('result fib(2):', fib(2))
// console.log('result fib(4):', fib(4))
// console.log('result fib(10):', fib(10))
// console.log('result fib(28):', fib(28))
// console.log('result fib(35):', fib(35))



function reverse(str) {
    if (str.length <= 1) return str
    return reverse(str.slice(1)) + str[0]

}

// console.log('result reverse(emosewa):', reverse('emosewa'))
// console.log('result reverse(loohcsmhtir):', reverse('loohcsmhtir'))

/**
 * 判断是否是回文
 * 每次都比较str的第一个和最后一个是否相同
 * @param {*} str 
 */
function isPalindrome(str) {
    if (str.length === 1) return true
    if (str.length === 2) return str[0] === str[1]
    if (str[0] !== str[str.length - 1]) return false
    return isPalindrome(str.slice(1, str.length - 1))
}

// console.log("result isPalindrome('abba'):", isPalindrome('abba'))
// console.log("result isPalindrome('awesome'):", isPalindrome('awesome'))
// console.log("result isPalindrome('amanaplanacanalpanama'):", isPalindrome('amanaplanacanalpanama'))

const isOdd = val => val % 2 !== 0

function someRecursive(arr, callback) {
    if (arr.length === 0) return false
    if (callback(arr[0])) return true
    return someRecursive(arr.slice(1), callback)
}

// console.log('result:someRecursive([1,2,3,4], isOdd)', someRecursive([1, 2, 3, 4], isOdd))
// console.log('result:someRecursive([4,6,8,9], isOdd)', someRecursive([4, 6, 8, 9], isOdd))
// console.log('result:someRecursive([4,6,8], isOdd)', someRecursive([4, 6, 8], isOdd))
// console.log('result:someRecursive([4,6,8], val => val > 10)', someRecursive([4, 6, 8], val => val > 10))

function flatten(arr) {
    let newArray = []
    for (let i = 0; i <= arr.length; i++) {
        if (Array.isArray(arr[i])) {
            newArray = newArray.concat(flatten(arr[i]))
        } else {
            newArray.push(arr[i])
        }
    }
    return newArray

}